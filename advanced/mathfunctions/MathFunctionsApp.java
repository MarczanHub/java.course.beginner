package advanced.mathfunctions;

public class MathFunctionsApp {
    public static void main(String[] args) {

        double temperatureYesterday = -5.6;
        double temperatureToday = Math.random() * 10;

        System.out.println("Absolute temperature toady: " + Math.abs(temperatureToday));
        //Math.abs wy�wietla zawsze liczb� dodatni�

        System.out.println("Min: " + Math.min(temperatureToday, temperatureYesterday));
        //Math.min zwraca najmniejsz� z dw�ch podanych warto�ci

        System.out.println("Max: " + Math.max(temperatureToday, temperatureYesterday));
        //Math.max zwraca najwi�ksz� z dw�ch podanych warto�ci

        System.out.println("Round: " + Math.round(temperatureToday));
        //Math.round podaje liczb� w przybli�eniu

        System.out.println("Ceiling: " + Math.ceil(temperatureToday));
        //Math.ceil wraca najmniejsz� liczb� ca�kowit� wi�ksz� od lub r�wn� danej.

        System.out.println("Floor: " + Math.floor(temperatureToday));
        //Math.floor zwraca najwi�ksz� liczb� ca�kowit� mniejsz� od lub r�wn� danej.

        System.out.println("Power: " + Math.pow(temperatureToday, 2));
        //Math.pow zwraca liczb� (parametr 1) podniesion� do pot�gi (parametr 2)

        System.out.println("Square root: " + Math.sqrt(Math.abs(temperatureToday)));
        //Math.sqrt zwraca pierwiastek kwadratowy danej liczby.
        /*Dodanie 'Math.abs' sprawia, �e zmienna 'temperatureToday' b�dzie zawsze wy�wietla� warto�� dodatni�,
        //a co za tym idzie � redukujemy do zera prawdopodobie�stwo wydrukowania warto�ci 'NaN' (Not a Number)*/
    }
}
