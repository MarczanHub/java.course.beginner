package intermediate.statickeyword;

public class CountriesApp {
    public static void main(String[] args) {
        System.out.println("Number of countries: " + Country.getNumberOfCountries());
        Country brazil = new Country("Brazil", 2100000, "Brasilia");
        brazil.getDetails();
        Country germany = new Country("Germany", 83000000, "Berlin");
        germany.getDetails();
        Country japan = new Country("Japan", 127000000, "Tokio");
        japan.getDetails();
        System.out.println("Number of countries: " + Country.getNumberOfCountries());
    }
}
