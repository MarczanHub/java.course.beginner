package intermediate.inheritance.car;

public class CarsApp {
    public static void main(String[] args) {
        DodgeChallenger redDodgeChallenger = new DodgeChallenger( "red", (byte) 3, (short) 450, (short) 717, (short) 7700, 40000, 150000, true);
        redDodgeChallenger.getDescription();
        redDodgeChallenger.startTheEngine();

        System.out.println("----------------------------------------------");
        ToyotaSupra blackToyotaSupra = new ToyotaSupra( "Black", (byte) 1, (short) 567, (short) 917, (short) 8700, 35000, 123000, false);
        blackToyotaSupra.getDescription();
        blackToyotaSupra.startTheEngine();
    }
}
