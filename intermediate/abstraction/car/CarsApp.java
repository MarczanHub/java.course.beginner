package intermediate.abstraction.car;

public class CarsApp {
    public static void main(String[] args) {
        DodgeChallenger redDodgeChallenger = new DodgeChallenger( "red", (byte) 3, (short) 450, (short) 717, (short) 7700, 40000, 150000, true);
        System.out.println(redDodgeChallenger.getCarName() + "'s price is $" + redDodgeChallenger.getPrice());
        redDodgeChallenger.setPrice(35000);
        System.out.println(redDodgeChallenger.getCarName() + "'s new price is $" + redDodgeChallenger.getPrice());
        System.out.println("The car is in city mode.");
        redDodgeChallenger.startTheElectricEngine(); //wy�wietla funkcj� interfejsu ElectricMode
        System.out.println("The car has " + redDodgeChallenger.getMileage() + " miles");
        redDodgeChallenger.drive(100);
        System.out.println("After driving the car, it has " + redDodgeChallenger.getMileage() + " miles");

        System.out.println("----------------------------------------------");
        ToyotaSupra blackToyotaSupra = new ToyotaSupra( "Black", (byte) 1, (short) 567, (short) 917, (short) 8700, 35000, 123000, false);
        System.out.println(blackToyotaSupra.getCarName() + "'s price is $" + blackToyotaSupra.getPrice());
        blackToyotaSupra.setPrice(43000);
        System.out.println(blackToyotaSupra.getCarName() + "'s new price is $" + blackToyotaSupra.getPrice());
        blackToyotaSupra.startTheEngine();
        System.out.println("The car has " + blackToyotaSupra.getMileage() + " miles");
        blackToyotaSupra.drive(1500);
        System.out.println("After driving the car, it has " + blackToyotaSupra.getMileage() + " miles");

    }
}
