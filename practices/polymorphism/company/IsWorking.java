package practices.polymorphism.company;

public interface IsWorking {
    void isWorking();
    void isWorking(String activityName);
}
