package practices.polymorphism.company;

public class ProductionWorker extends Worker {

    private String technicalEducation;
    private int professionalExperience;

    public ProductionWorker(String name, String surname, int age, String department, int salary, boolean workQuality, String technicalEducation, int professionalExperience) {
        super(name, surname, age, department, salary, workQuality);
        this.technicalEducation = technicalEducation;
        this.professionalExperience = professionalExperience;
    }

    @Override
    public void getBio() {
        super.getBio();
        System.out.println("Technical education: " + technicalEducation);
        System.out.println("Professional experience: " + professionalExperience + " years of work");
    }

    @Override
    public String getEmail() {
        return "E-mail: " + getName() + "." + getSurname() + "@clermont.com";
    }

    public void setProfessionalExperience(int professionalExperience) {
        this.professionalExperience = Math.abs(professionalExperience);
    }
}
