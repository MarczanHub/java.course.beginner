package practices.polymorphism.company;

public class WorkerApp {

    public static void main(String[] args) {

        QualityWorker john = new QualityWorker("John", "Shower", 39, "Quality", 3000, "English C1", "ISO 9001", false);
        john.setSalary(5000);
        QualityWorker gregory = new QualityWorker("Gregory", "Caperton", 28, "Quality", 2800, "English B2", "ISO 9001", true);
        QualityWorker anna = new QualityWorker("Anna", "Smith", 32, "Quality Inspection", 4500, "English C1, French B2", "ISO 9001, ISO 5001, ISO 16001", true);
        anna.setLanguageSkills("English C3, French C1");
        anna.setSalary(12000);

        QualityWorker[] qualityWorkers = new QualityWorker[3];
        qualityWorkers[0] = john;
        qualityWorkers[1] = gregory;
        qualityWorkers[2] = anna;

        for (QualityWorker qualityWorker : qualityWorkers) {
            System.out.println("\n-----------------------------\n");
            qualityWorker.getBio();
            qualityWorker.isWorking();
            qualityWorker.isWorking("checking production final products.");
            System.out.println(qualityWorker.getEmail().toLowerCase());
        }

        ProductionWorker simon = new ProductionWorker("Simon", "Jacks", 29, "Production", 1500, false, "None", 5);
        simon.setProfessionalExperience(6);
        ProductionWorker andrew = new ProductionWorker("Andrew", "Grocer", 55, "Maintenance", 2500, true, "Maintenance technician", 15);
        andrew.setProfessionalExperience(20);
        ProductionWorker elena = new ProductionWorker("Elena", "Scott", 37, "Warehouse", 1900, true, "MA in humanities", 3);
        elena.setSalary(2300);

        ProductionWorker[] productionWorkers = new ProductionWorker[3];
        productionWorkers[0] = simon;
        productionWorkers[1] = andrew;
        productionWorkers[2] = elena;

        for (ProductionWorker productionWorker : productionWorkers) {
            System.out.println("\n-----------------------------\n");
            productionWorker.getBio();
            System.out.println(productionWorker.getEmail().toLowerCase());
        }
    }
}
