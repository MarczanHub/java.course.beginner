package practices.apps;

public class Primitives_Practice {
    public static void main(String[] args) {
        String brandName = "KLEENGUARD";
        String color = "Smoke Gray Body, Smoke Lens, Black Frame";
        long eanNumber = 1234567891L;
        String finishType = "anti-fog";
        float itemWeight = 0.960f;
        String material = "Polycarbonate Lens";
        int modelNumber = 49311;
        byte numberOfItems = 1;
        int unspscCode = 42142900;

        System.out.println("\nKLEENGUARD KCC49311, Maverick Safety Eyewear, 1" +
                " / Each, Smoke Gray Body,Smoke Lens,Black Frame\n");

        System.out.println("Brand name:\t" + brandName);
        System.out.println("Color:\t" + color);
        System.out.println("EAN: \t" + eanNumber);
        System.out.println("Finish type:\t" + finishType);
        System.out.println("Item weight:\t" + itemWeight);
        System.out.println("Material:\t" + material);
        System.out.println("Model number:\t" + modelNumber);
        System.out.println("Number of items:\t" + numberOfItems);
        System.out.println("UNSPSC Code:\t" + unspscCode);
    }
}
