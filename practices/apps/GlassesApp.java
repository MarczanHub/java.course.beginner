package practices.apps;

public class GlassesApp {
    public static void main(String[] args) {

        Glasses kleenguardSmokeGray = new Glasses("KLEENGUARD","Smoke Gray, Smoke Lens, Black Frame", "Anti-fog", "Polycarbonate Lens", "Safety Glasses", "Universal", 49311, 42142900, 7.35, 1234567891L, 0.960f);
        kleenguardSmokeGray.getDescription();

        System.out.println("----------------------------------------------");
        Glasses kleenguardCarbonBlack = new Glasses("KLEENGUARD","Carbon Black, Smoke Lens, Carbon Black Frame", "Anti-fog", "Polycarbonate Lens", "Safety Glasses", "Universal", 41856, 52342970, 19.90, 3574567891L, 0.830f);
        kleenguardCarbonBlack.getDescription();
    }
}
