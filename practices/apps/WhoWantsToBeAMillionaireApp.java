package practices.apps;

import java.util.Scanner;

public class WhoWantsToBeAMillionaireApp {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Who is named as the father of the light bulb?\n");
        System.out.println("A: Nikola Tesla\t" + "\tB: Marie Curie" + "\nC: Thomas Edison" + "\tD: Robert Oppenheimer\n");
        String usersAnswer = sc.next();

        switch (usersAnswer) {
            case "A", "a", "B", "b", "D", "d" -> System.out.println("Incorrect answer.");
            case "C", "c" -> {
                System.out.println("Congratulations! You're right!");
                System.out.println("You won $100. Thank you for coming.");
            }
            default -> System.out.println("Not defined answer");
        }

        }
    }
