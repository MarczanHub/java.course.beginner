package practices.apps;

public class Glasses {

    String brandName;
    String color;
    String finishType;
    String material;
    String includedComponents;
    String size;

    int modelNumber;
    int unspscCode;

    double price;

    long eanNumber;
    float itemWeight;


    public Glasses(String brandName, String color, String finishType, String material, String includedComponents, String size, int modelNumber, int unspscCode, double price, long eanNumber, float itemWeight) {
        this.brandName = brandName;
        this.color = color;
        this.finishType = finishType;
        this.material = material;
        this.includedComponents = includedComponents;
        this.size = size;
        this.modelNumber = modelNumber;
        this.unspscCode = unspscCode;
        this.price = price;
        this.eanNumber = eanNumber;
        this.itemWeight = itemWeight;
    }

    public void getDescription() {

        System.out.println("\nBrand name: " + brandName);
        System.out.println("Color: " + color);
        System.out.println("Finish type: " + finishType);
        System.out.println("Material: " + material);
        System.out.println("Included components: " + includedComponents);
        System.out.println("Size: " + size);
        System.out.println("Model number: " + modelNumber);
        System.out.println("UNSPSC Code: " + unspscCode);
        System.out.println("EAN number: " + eanNumber);
        System.out.println("Item weight: " + itemWeight + " ounces\n");
        System.out.println("Price: $" + price);
    }
}
