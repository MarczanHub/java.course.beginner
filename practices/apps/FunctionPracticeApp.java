package practices.apps;

public class FunctionPracticeApp {
    public static void main(String[] args) {
        System.out.println(energyEfficientCategory('z'));
        carDescription("Tesla Roadster", 2018, "good");
    }
    static String energyEfficientCategory (char category){



        switch (category){
            case 'A','a':
                return "Very low";
            case 'B', 'b':
                return "Low";
            case 'C', 'c':
                return "Normal";
            case 'D', 'd':
                return "Above normal";
            case 'E', 'e':
                return "High";
            case 'F', 'f':
                return "Very high";
            case 'G', 'g':
                return "Extremely";

            default:
                return "invalid energy efficiency category ";

        }
    }
    static void carDescription (String model, int productionYear, String condition){
        System.out.println("This car is a " + model);
        System.out.println("Production year: " + productionYear);
        switch (condition){
            case "good":
                System.out.println("It's in good condition");
                break;
            case "bad":
                System.out.println("It's in bad condition - needs some repairs");
                break;
            case "damaged":
                System.out.println("Car is damaged");
                break;
            default:
                System.out.println("unknown condition statement");
        }
    }
}
