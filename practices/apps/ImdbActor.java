package practices.apps;

import java.util.Arrays;

public class ImdbActor {
    public static void main(String[] args) {

        String name = "Johnny";
        String surname = "Depp";
        int yearOfBorn = 1963;

        System.out.println("Actor's name: " + name + " " + surname);
        System.out.println("Year of born: " + yearOfBorn);
        System.out.println("Age: " + (2022 - yearOfBorn) + " years old");
        System.out.println("Movies:");

        String[] movieTitles = new String[3];
        movieTitles[0] = "Pirates of the Caribbean";
        movieTitles[1] = "Sweeney Todd: The Demon Barber of Fleet Street";
        movieTitles[2] = "Charlie and the Chocolate Factory";

        float[] movieRatings = new float[3];
        movieRatings[0] = 8.1F;
        movieRatings[1] = 7.3F;
        movieRatings[2] = 6.6F;

        for (int i = 0; i < movieRatings.length; i++) {
            System.out.println(movieTitles[i] + " | Movie rating: " + movieRatings[i] + " - " + getRating(movieRatings[i]));

        }
    }

    public static String getRating(float rating) {

        if (rating <= 5.0) {
            return "bad";
        } else if (rating > 5.0 && rating <= 6.5) {
            return "average";
        } else if (rating > 6.5 && rating <= 7.0) {
            return "good";
        } else if (rating > 7.0 && rating <= 8.0) {
            return "very good";
        } else {
            return "amazing";
        }
    }

}
    
