package practices.abstraction.cars;

public class VolkswagenPolo extends Car {

    private String owner;
    public VolkswagenPolo(String brandName, String model, String type, String color, int firstRegistrationDate, int lastRegistrationDate, int productionYear, int totalMass, double engineCapacity, String owner) {
        super(brandName, model, type, color, firstRegistrationDate, lastRegistrationDate, productionYear, totalMass, engineCapacity);
        this.owner = owner;
    }

    @Override
    public void getDescription() {
        super.getDescription();
        System.out.println("Owner of vehicle: " + owner);
    }

    @Override
    public void startTheEngine() {
        System.out.print("Starting car's engine:");
        System.out.println(" Vroooooom, vroom!");
    }
}
