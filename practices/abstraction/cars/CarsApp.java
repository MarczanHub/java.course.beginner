package practices.abstraction.cars;

public class CarsApp {
    public static void main(String[] args) {

        VolkswagenPolo volkswagenPolo = new VolkswagenPolo("Volkswagen", "Polo", "Hatchback", "silver", 2003, 2018, 2003, 1630, 1896.0, "Marek");
        volkswagenPolo.getDescription();
        volkswagenPolo.startTheEngine();

    }
}
