package practices.abstraction.cars;

public abstract class Car {

    private String brandName;
    private String model;
    private String type;
    private String color;
    private int firstRegistrationDate;
    private int lastRegistrationDate;
    private int productionYear;
    private int totalMass;
    private double engineCapacity;

    public Car(String brandName, String model, String type, String color, int firstRegistrationDate, int lastRegistrationDate, int productionYear, int totalMass, double engineCapacity) {
        this.brandName = brandName;
        this.model = model;
        this.type = type;
        this.color = color;
        this.firstRegistrationDate = firstRegistrationDate;
        this.lastRegistrationDate = lastRegistrationDate;
        this.productionYear = productionYear;
        this.totalMass = totalMass;
        this.engineCapacity = engineCapacity;
    }

    public void getDescription() {
        System.out.println("Brand name: " + brandName);
        System.out.println("Car model: " + model);
        System.out.println("Car type: " + type);
        System.out.println("Color: " + color);
        System.out.println("Engine capacity: " + engineCapacity + " cm3");
        System.out.println("Production year: " + productionYear);
        System.out.println("Total mass of vehicle: " + totalMass + " kg");
        System.out.println("Date of first registration: " + firstRegistrationDate);
        System.out.println("Date of last registration: " + lastRegistrationDate);
    }

    public void setColor(String color) {
        this.color = color;
    }

    public abstract void startTheEngine ();
}
