package practices.abstraction.company;

public class QualityWorker extends Worker implements IsWorking {

    private String languageSkills;
    private String certification;

    public QualityWorker(String name, String surname, int age, String department, int salary, String languageSkills, String certification, boolean workQuality) {
        super(name, surname, age, department, salary, workQuality);
        this.languageSkills = languageSkills;
        this.certification = certification;
    }

    @Override
    public void getBio() {
        super.getBio();
        System.out.println("Language knowledge: " + languageSkills + " level");
        System.out.println("Certification: " + certification);
    }

    @Override
    public String getEmail() {
        return "E-mail: " + getName() + "." + getSurname() + "@clermont.com";
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }

    @Override
    public void isWorking() {
        System.out.println("Status: " + getName() + " " + getSurname() + " is working now.");
    }
}

