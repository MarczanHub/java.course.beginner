package practices.encapsulation;

public class WorkerApp {

    public static void main(String[] args) {

        QualityWorker john = new QualityWorker("John", "C. Shaw", 39, "Quality", 3000, "English C1", "ISO 9001", false);
        john.getBio();
        john.setSalary(5000);
        System.out.println("\n" + john.getName() + "'s salary has been raised.");
        System.out.println(john.getName() + "'s actual salary is equal to: $" + john.getSalary());

        QualityWorker gregory = new QualityWorker("Gregory", "Y. Caperton", 28, "Quality", 2800, "English B2", "ISO 9001", true);
        gregory.getBio();

        QualityWorker anna = new QualityWorker("Anna", "Smith", 32, "Quality Inspection", 4500, "English C1, French B2", "ISO 9001, ISO 5001, ISO 16001", true);
        anna.setLanguageSkills("English C3, French C1");
        anna.getBio();
        anna.setSalary(12000);
        System.out.println("\n" + anna.getName() + "'s salary has been raised.");
        System.out.println(anna.getName() + "'s actual salary is equal to: $" + anna.getSalary());

/*        QualityWorker[] qualityWorkers = new QualityWorker[3];
        qualityWorkers[0] = john;
        qualityWorkers[1] = gregory;
        qualityWorkers[2] = anna;

        for (QualityWorker qualityWorker : qualityWorkers) {
            System.out.println("-----------------------------\n");
            qualityWorker.getBio();
            qualityWorker.getDevelopmentOpportunities();
        }*/

        ProductionWorker simon = new ProductionWorker("Simon", "Jacks", 29, "Production", 1500, false, "None", 5);
        simon.setProfessionalExperience(6);
        simon.getBio();
        simon.getDevelopmentOpportunities();
        ProductionWorker andrew = new ProductionWorker("Andrew", "Grocer", 55, "Maintenance", 2500, true, "Maintenance technician", 15);
        andrew.setProfessionalExperience(20);
        andrew.getBio();
        ProductionWorker elena = new ProductionWorker("Elena", "Scott", 37, "Warehouse", 1900, true, "MA in humanities", 3);
        elena.getBio();

/*        ProductionWorker[] productionWorkers = new ProductionWorker[3];
        productionWorkers[0] = simon;
        productionWorkers[1] = andrew;
        productionWorkers[2] = elena;

        for (ProductionWorker productionWorker : productionWorkers) {
            System.out.println("-----------------------------\n");
            productionWorker.getBio();
            productionWorker.getDevelopmentOpportunities();
        }*/
    }
}
