package practices.encapsulation;

public class Worker {

    private String name;
    private String surname;
    private int age;
    private String department;
    private int salary;

    private boolean workQuality;

    public Worker(String name, String surname, int age, String department, int salary, boolean workQuality) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.department = department;
        this.salary = salary;
        this.workQuality = workQuality;
    }

    public void getBio() {
        System.out.println("\nClermont Automotive emoloyee:\n");
        System.out.println("Name: " + name);
        System.out.println("Surname: " + surname);
        System.out.println("Age: " + age + " years old");
        System.out.println("Department: " + department + " department");
        System.out.println("Month salary: $" + salary);
    }

    public void getDevelopmentOpportunities (){
        System.out.println("\nFuture development opportunities for " + name + " " + surname + ", based on work quality:");
        if (workQuality){
            System.out.println("25% salary increase\n");
        } else {
            System.out.println("None\n");
        }
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = Math.abs(salary);
    }
}
