package practices.encapsulation;

public class QualityWorker extends Worker {

    private String languageSkills;
    private String certification;

    public QualityWorker(String name, String surname, int age, String department, int salary, String languageSkills, String certification, boolean workQuality) {
        super(name, surname, age, department, salary, workQuality);
        this.languageSkills = languageSkills;
        this.certification = certification;
    }

    @Override
    public void getBio() {
        super.getBio();
        System.out.println("Language knowledge: " + languageSkills + " level");
        System.out.println("Certification: " + certification);
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }
}
