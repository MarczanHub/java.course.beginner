package beginner;

public class ObjectReferenceApp {
    public static void main(String[] args) {
        Byte numberOfSeats = 5;
        Short horsePower = 392;
        Integer price = 14999;
        Long registrationNumber = 2323424234234L;

        Float fuelConsumptionCombined = 15.5F;
        Double fuelConsumptionPrecise = 15.53414123;

        Boolean isDamaged = false;
        Character energyEfficientCategory = 'G';

        String carModel = "Dodge Challenger SRT 392";
        String carModelNew = "Dodge Challenger SRT 392";

        System.out.println("Number of seats: " + numberOfSeats);
        System.out.println("Horsepower: " + horsePower);
        System.out.println("Price: $" + price.floatValue());
        System.out.println("Registration Number: " + registrationNumber);
        System.out.println("Fuel Consumption Combined: " + fuelConsumptionCombined);
        System.out.println("Fuel Consumption Precise: " + fuelConsumptionPrecise.intValue());
        System.out.println("Is Damaged: " + isDamaged);
        System.out.println("Energy Efficient Category: " + energyEfficientCategory);
        System.out.println("Car Model: " + carModel);
        System.out.println("Car Model New: " + carModelNew);

        String damageText = isDamaged ? "The car is damaged." : "The car isn't damaged";
        System.out.println(damageText);
    }
}
